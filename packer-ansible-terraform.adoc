= Packer - Ansible - Terraform
Arkadiusz Tułodziecki <atulodzi@gmail.com>
1.0, Apr 24, 2021: AsciiDoc article template
:toc:
:source-highlighter: rouge
:encoding: utf-8
:icons: font
:url-quickref: https://docs.asciidoctor.org/asciidoc/latest/syntax-quick-reference/

ifdef::backend-html5[]
PDF version of this article:
link:packer-ansible-terraform.pdf/[packer-ansible-terraform.pdf]
endif::[]

== Immutable Infrastructure

We will be creating immutable images thanks to Packerfootnote:[Machine images automation tool https://www.packer.io/]
which will be using provisioners like Shellfootnote:[Shell Provisioner https://www.packer.io/docs/provisioners/shell] and Ansiblefootnote:[Configuration management tool https://docs.ansible.com/ used here as the provisioner https://www.packer.io/docs/provisioners/ansible] to configure images and will create machine images using buildersfootnote:[Packer Builders https://www.packer.io/docs/builders] for platforms we will choose.

We will be using a provisioning tool such as Terraformfootnote:[Provisioning tool https://www.terraform.io/] to deploy machine images created by Packer to those platforms.

.For example, to deploy a new version of OpenSSL, you would:
* use packer with Ansible provider to create a new image with the new version of OpenSSL
* deploy that image across a set of new servers
* terminate the old servers.

With this method, every deployment uses created by packer immutable images on fresh servers.

[quote,Yevgeniy Brikman,Terraform: Up &amp; Running - 2nd Edition]
____
This approach reduces the likelihood of configuration drift bugs, makes it easier to know exactly what software is running on each server, and allows you to easily deploy any previous version of the software (any previous image) at any time. It also makes your automated testing more effective because an immutable image that passes your tests in the test environment is likely to behave the same way in the production environment.
____

We will also discuss how to use packer in a non-standard way to test our Ansible playbook and roles. There are better methods to test Ansible roles and playbooks, but we will see how in the simple way we can create CI to just check if our playbook and roles will not fail.

== Packer

For example, our packer file `packer-template-playbook.json` which generates AMIs in the AWS cloud can look like this:

[source,json]
----
include::materials/packer-template-playbook.json[]
----

.Where we can have:
* *variables* - which are very useful when used with the CI - in the current example GitLab CI,
* *builders* - in our example we are searching for the latest Debian 10 AMI to be used as the core AMI and using our Security Group and IAM Instance profile to have as much security we need for this process,
* *provisioners* - in our example we have two - shell which will update AMI and Ansible which will do work we need to configure our AMI
* *post-processors* - which in our case will generate `manifest.json` file with very useful data which can be used later in the CI.

In this case, we are assuming that our Ansible playbook have all ansible-galaxy roles and special roles we prepared just for it. To be sure, that playbook will have what is needed we can in our CI have a script which will be started first like for example this one:

[source,bash]
----
include::materials/pleybook-refresh-roles.sh[]
----

where our `requirements.yml` can look like this:

[source,yaml]
----
include::materials/playbook-requirements.yml[]
----

and our `packer-playbook.yml` can look like this:

[source,yaml]
----
include::materials/packer-playbook.yml[]
----

We can validate our packer file with:

[source,bash]
----
packer validate packer-template-playbook.json
----

and then we can build AMI just with:

[source,bash]
----
packer build packer-template-playbook.json
----

If there will be no failures in the result, we will have working AMI.

== Ansible

In the previous example we were using two common roles and one role specific to this playbook. It is good to create common roles when there is a code used in many playbooks, but we always need to remember that those roles should be simple. Do not try to create something what will work on every possible distribution, and its version. Occasionally, it is better to copy code between roles just to not complicate them too much.

We like now to have some kind of the way to test them without running a big and long-running building process. For simplicity let's assume there is no need of running them in specific order. Let's take `common.base` as the example.

First, we need to prepare our role to be tested like a playbook with our `preparation.sh` script:

[source,bash]
----
include::materials/preparation.sh[]
----

Second, we need to have packer file which will test our role in the similar environment we are planning to use it `packer-template-role.json`:

[source,json]
----
include::materials/packer-template-role.json[]
----

And then our playbook file will look like this:
[source,yaml]
----
include::materials/packer-role.yml[]
----

It is not an ideal testing method and there are better ones used by the most open sourced roles, but I found this very useful way of at least having roles tested. A good idea is also to have those tests scheduled like for example once a month to be sure the role is still working properly.

image:https://i.creativecommons.org/l/by/4.0/88x31.png[Creative Commons Licence,88,31] This work is licensed under a http://creativecommons.org/licenses/by/4.0/[Creative Commons Attribution 4.0 International License]
